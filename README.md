[![build status](https://gitlab.com/toeknee/bookpicker/badges/master/build.svg)](https://gitlab.com/toeknee/bookpicker/commits/master)
# bookpicker

Get free books from [Packt](https://www.packtpub.com/packt/offers/free-learning)
