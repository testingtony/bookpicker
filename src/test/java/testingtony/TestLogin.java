package testingtony;

import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Random;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestLogin {

    @Rule
    public DriverManager driverManager = new DriverManager.RC(DesiredCapabilities.chrome());

    @Test
    public void gettingBookTakesMeToTheMyBooksPage() throws InterruptedException {
        FreeBooksPage loginPage = new FreeBooksPage(driverManager.getDriver());
        String username = System.getenv("login");
        String password = System.getenv("password");

        loginPage.get();
        loginPage.closePopupIfExists();
        loginPage.login(username, password);
        String bookTitle = loginPage.getBookTitle();
        System.err.println("Book Title is " + bookTitle);
        loginPage.closePopupIfExists();
        loginPage.claim();

        assertThat(driverManager.getDriver().getCurrentUrl(), equalTo("https://www.packtpub.com/account/my-ebooks"));
    }

    public void followRandomLinks(FreeBooksPage page) {
        int n = new Random().nextInt(4) + 2;
        for(int i=0; i<n; i++) {
            page.gotToRandomPageAndBack();
        }
    }
}
