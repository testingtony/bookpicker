package testingtony;

import org.apache.commons.io.FileUtils;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public abstract class DriverManager implements TestRule {
    private WebDriver driver;

    abstract WebDriver setDriver() ;

    public final WebDriver getDriver() {
        return driver;
    }

    @Override
    public Statement apply(final Statement statement, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                driver = setDriver();
                try {
                    before(driver);
                    statement.evaluate();
                    after(driver);
                } catch (Throwable e) {
                    if (driver instanceof TakesScreenshot) {
                        screenshot(description.getMethodName());
                    }
                    throw e;
                } finally {
                    driver.quit();
                }
            }
        };

    }

    public void before(WebDriver driver) {
    }

    public void after(WebDriver driver) {
    }

    final public void screenshot(String id) {
        try {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            File dir = new File("target/screenshots");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File result = File.createTempFile("screenshot_" + id + "_", ".png", dir);
            FileUtils.copyFile(scrFile, result);
            System.err.println("[[ATTACHMENT|" + result.getAbsolutePath()+"]]");
        } catch (Exception e) {
            System.err.println();
            System.err.println("This is embarassing");
            e.printStackTrace();
            System.err.println();
        }
    }

    public static class Chrome extends DriverManager {
        @Override
        WebDriver setDriver() {
            return new ChromeDriver();
        }
    };

    public static class Firefox extends DriverManager {
        @Override
        WebDriver setDriver() {
            return new FirefoxDriver();
        }
    };

    public static class HtmlUnit extends DriverManager {
        @Override
        WebDriver setDriver() {
            return new HtmlUnitDriver();
        }
    }

    public static class RC extends DriverManager {
        private final DesiredCapabilities desiredCapabilities;

        public RC(DesiredCapabilities desiredCapabilities) {
            this.desiredCapabilities = desiredCapabilities;
        }

        @Override
        WebDriver setDriver() {
            String remoteHost = System.getProperty("grid.host", "localhost");
            String remotePort = System.getProperty("grid.port", "4444");
            String remoteUrl = System.getProperty("grid.url", "http://" + remoteHost + ":" + remotePort +"/wd/hub" );
            try {
                return new RemoteWebDriver(new URL(remoteUrl), desiredCapabilities);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static boolean waitUntilFound(WebElement... elements) {
        return waitUntilFound(20, elements);
    }

    public static boolean waitUntilFound(int timeOut, WebElement... elements) {
        long timeStarted = System.currentTimeMillis();
        long timeToWaitInMillis = timeOut * 1000L;

        for(WebElement element : elements) {
            while (true) {
                long timeNow = System.currentTimeMillis();
                if (timeNow - timeStarted > timeToWaitInMillis) {
                    return false;
                }

                String toString = element.toString();
                if (!toString.startsWith("Proxy element for: ")) {
                    break;
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

}
