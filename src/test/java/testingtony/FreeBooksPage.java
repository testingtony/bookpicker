package testingtony;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;

public class FreeBooksPage extends LoadableComponent<FreeBooksPage> {
    private WebDriver driver;

    @FindBy(id="menuIcon")
    WebElement menuIcon;

    @FindBy(css="div.respoPage")
    WebElement menu;

    @FindBy(id="div.respoAcc")
    WebElement loggedInIndicator;

    @FindBy(css="div.respoLogin")
    WebElement loginButton;

    @FindBy(id="email")
    WebElement emailInput;

    @FindBy(id="password")
    WebElement passwordInput;

    @FindBy(id="edit-submit-1")
    WebElement loginSubmit;

    @FindBy(css="div.dotd-title h2")
    WebElement bookTitle;

    @FindBy(css="div.dotd-main-book-image a")
    WebElement bookImage;

    By claimButtonSelector = By.cssSelector(".twelve-days-claim input.form-submit");

    @FindBy(id="popup-recaptcha")
    WebElement recaptcha;

    @FindBy(css="div.menu-item-text")
    List<WebElement> menuItems;

    // @FindBy(css="w-div[role='dialog'] span")
    @FindBy(css="w-div span")
    WebElement annoyingPopupDismiss;

    public FreeBooksPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @Override
    protected void load() {
        driver.manage().window().setSize(new Dimension(800,1200));
        driver.get("https://www.packtpub.com/packt/offers/free-learning");
    }

    @Override
    protected void isLoaded() throws Error {
        String url = driver.getCurrentUrl();
        assertThat("Not on the home page: " + url, url.matches("https://www.packtpub.com/packt/offers/free-learning"));
    }

    public boolean isLoggedIn() {
        return DriverManager.waitUntilFound(2, loggedInIndicator);
//        return loggedInIndicator.isDisplayed();
    }

    public void showResponsiveMenu() {
        if ("hidden".equals(menu.getAttribute("state"))) {
            menuIcon.click();
        }
    }

    public void hideResponsiveMenu() {
        if ("show".equals(menu.getAttribute("state"))) {
            menuIcon.click();
        }
    }

    public String getBookTitle() {
        return bookTitle.getText();
    }

    public void login(String username, String password) {
        showResponsiveMenu();
        sleepRandom(500,1000);
        if (isLoggedIn()) {
            hideResponsiveMenu();
            sleepRandom(500, 1000);
            return;
        }

        loginButton.click();

        sleepRandom(500, 1000);
        emailInput.clear();
        emailInput.sendKeys(username);
        sleepRandom(500, 1000);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        sleepRandom(500, 1000);
        loginSubmit.click();
        sleepRandom(500, 1000);
        hideResponsiveMenu();
    }

    public void clickRecaptchaIfExists() {
        if (DriverManager.waitUntilFound(recaptcha)) {
            try {

                Actions builder = new Actions(driver);
                WebElement claimButton = new WebDriverWait(driver, 10)
                        .until(ExpectedConditions.elementToBeClickable(claimButtonSelector));
                Random r = new Random();
                Thread.sleep(750 + r.nextInt(1000));
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", recaptcha);
                builder = builder.moveToElement(claimButton, r.nextInt(10), r.nextInt(10));
                int x = 0;
                int y = 0;
                int destX = 50 + r.nextInt(10);
                int destY = 57 + r.nextInt(10);
                while( x < destX && y < destY) {
                    int dx = r.nextInt(5);
                    int dy = r.nextInt(5);
                    builder = builder.moveByOffset(dx, dy);
                    x += dx;
                    y += dy;
                }
                System.out.println("Clicking at " + x + "," + y);
                builder = builder.click();
                builder.build().perform();
                Thread.sleep(2000 + r.nextInt(2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void claim() {
        WebElement claimButton = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(claimButtonSelector));

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        claimButton.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void lookAtBook() {
        Random r = new Random();

        if (r.nextInt(3) == 0) {
            return;
        }

        sleepRandom();
        Actions actions = new Actions(driver);

        actions.moveToElement(bookImage).click().perform();
        sleepRandom();
        driver.navigate().back();
        sleepRandom();
    }




    public void sleepRandom() {
        sleepRandom(500, 2500);
    }
    public void sleepRandom(int lower, int upper) {
        Random r = new Random();
        int v = lower + r.nextInt(upper - lower);
        try {
            Thread.sleep(v);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private List<String>getMenuItemsIds() {
        List<String> items = new LinkedList<String>();
        for (WebElement e: menuItems){
            String id = e.getAttribute("id");
            if (id.contains("_respo"))
                items.add(id);
        }
        return items;
    }

    private List<WebElement> getMenuItemsFor(String id) {
        WebElement menuItem = driver.findElement(By.id(id));
        menuItem.click();
        List<WebElement> subMenus = driver.findElements(By.cssSelector("p#_respo[style*='display: block'] a"));
        return subMenus;
    }

    public void gotToRandomPageAndBack() {
        showResponsiveMenu();
        String menu = getRandomItem(getMenuItemsIds());
        System.out.println("Selecting "  + menu);

        WebElement randomLink = getRandomItem(getMenuItemsFor(menu));
        if (randomLink == null)
            return;

        System.out.println("Selecting " + randomLink.getAttribute("href"));

        sleepRandom();
        randomLink.click();
        sleepRandom();
        driver.navigate().back();
        sleepRandom();

    }

    public void closePopupIfExists() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        try {
            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(annoyingPopupDismiss));
            element.click();
            Thread.sleep(1000);
        } catch (TimeoutException e) {
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private <T> T getRandomItem(List<T> items) {
        if (items.isEmpty())
            return null;
        int rand = new Random().nextInt(items.size());
        return items.get(rand);
    }

}
